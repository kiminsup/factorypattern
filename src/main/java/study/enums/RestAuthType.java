package study.enums;

public enum RestAuthType {
    OAUTH2, BASIC_AUTH
}
