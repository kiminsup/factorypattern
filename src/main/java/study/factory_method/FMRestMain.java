package study.factory_method;

import study.enums.RestAuthType;

public class FMRestMain {
    public static void main(String[] args) {
        // case OAuth2
        FMRestClient oAuthClient = FMRestClientFactory.getRestClient("10.10.10.10", 8080, "id", "pass", RestAuthType.OAUTH2);
        String data = oAuthClient.collect();
        System.out.println(data);

        FMRestClient basicClient = FMRestClientFactory.getRestClient("10.10.10.10", 8080, "id", "pass", RestAuthType.BASIC_AUTH);
        String basicData = basicClient.collect();
        System.out.println(basicData);

    }
}
