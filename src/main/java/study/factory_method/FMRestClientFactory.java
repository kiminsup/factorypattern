package study.factory_method;

import study.enums.RestAuthType;

public class FMRestClientFactory {
    public static FMRestClient getRestClient(String address, int port, String id, String password, RestAuthType authType) {
        FMRestClient client = null;

        switch (authType) {

            case OAUTH2 -> {
                client = new FMOAuth2RestClient(address, port, id, password);
            }
            case BASIC_AUTH -> {
                client = new FMBasicRestClient(address, port, id, password);
            }
        }

        boolean auth = client.auth();
        if (auth) {
            System.out.println("AUTH SUCCESS");
            return client;
        } else
            return null;
    }
}
