package study.factory_method;

public class FMBasicRestClient extends FMRestClient {

    public FMBasicRestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }

    @Override
    public boolean auth() {

        // Generate Basic Auth Key
        // Adding Header Basic Auth Key

        if (true) {
            log("Basic AUTH : ALSKDJLSKADJ");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String collect() {
        return "{\"data\":\"basic_data_body\"}";
    }

}
