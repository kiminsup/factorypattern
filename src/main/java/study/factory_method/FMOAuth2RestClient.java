package study.factory_method;

public class FMOAuth2RestClient extends FMRestClient {

    public FMOAuth2RestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }

    @Override
    public boolean auth() {

        // Get Token
        // Adding Header Basic Auth Key

        if (true) {
            log("TOKEN : ASDF");
            return true;
        } else {
            return false;
        }
    }

    public String collect() {
        return "{\"data\":\"oauth_data_body\"}";
    }

}
