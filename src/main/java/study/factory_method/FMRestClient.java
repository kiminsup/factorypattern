package study.factory_method;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
public abstract class FMRestClient {
    String address;
    int port;
    String id;
    String password;

    public abstract boolean auth();

    public abstract String collect();

    public void log(String logString) {
        System.out.println(new Date().getTime() + ":" + logString);
    }
}
