package study.factory_non;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
public class NonBasicRestClient {
    private String address;
    private int port;
    private String id;
    private String password;

    public NonBasicRestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }

    public boolean auth() {

        // Generate Basic Auth Key
        // Adding Header Basic Auth Key

        if (true) {
            return true;
        } else {
            return false;
        }
    }

    public String collect() {
        return "{\"data\":\"basic_data_body\"}";
    }

    public void log(String logString) {
        System.out.println(new Date().getTime() + ":" + logString);
    }
}
