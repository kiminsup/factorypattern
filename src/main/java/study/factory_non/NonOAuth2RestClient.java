package study.factory_non;

import lombok.Data;

import java.util.Date;

@Data
public class NonOAuth2RestClient {
    private String address;
    private int port;
    private String id;
    private String password;

    public NonOAuth2RestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }



    public boolean auth() {

        // Get Token
        // Adding Header Basic Auth Key

        if (true) {
            return true;
        } else {
            return false;
        }
    }

    public String collect() {
        return "{\"data\":\"oauth_data_body\"}";
    }

    public void log(String logString) {
        System.out.println(new Date().getTime() + ":" + logString);
    }
}
