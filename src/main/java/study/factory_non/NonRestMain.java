package study.factory_non;

public class NonRestMain {
    public static void main(String[] args) {
        // case OAuth2
        NonOAuth2RestClient oAuthClient =
                new NonOAuth2RestClient("10.10.10.10", 8080, "admin", "password");
        boolean oAuthconnect = oAuthClient.auth();
        System.out.println(oAuthconnect);
        String data = oAuthClient.collect();
        System.out.println(data);

        NonBasicRestClient basicClient =
                new NonBasicRestClient("10.10.10.10", 8080, "admin", "password");
        boolean basicconnect = basicClient.auth();
        System.out.println(basicconnect);
        String basicData = basicClient.collect();
        System.out.println(basicData);
    }
}
