package study.factory_abstract;

import study.enums.RestAuthType;
import study.enums.SDCType;
import study.factory_abstract.rest.FARestClient;
import study.factory_abstract.rest.FARestClientFactory;

public class FARestMain {
    public static void main(String[] args) {
        // case OAuth2
        FARestClient oAuthClient = FARestClientFactory.getRestClient("10.10.10.10", 8080, "id", "pass", SDCType.VCENTER);
        String data = oAuthClient.collect();
        System.out.println("COLLECTED DATA is [" + data + "]");

        System.out.println();
        System.out.println();

        FARestClient basicClient = FARestClientFactory.getRestClient("10.10.10.10", 8080, "id", "pass", SDCType.NUTANIX);
        String basicData = basicClient.collect();
        System.out.println("COLLECTED DATA is [" + basicData + "]");

    }
}
