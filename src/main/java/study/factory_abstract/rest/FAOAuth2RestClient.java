package study.factory_abstract.rest;

public class FAOAuth2RestClient extends FARestClient {

    public FAOAuth2RestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }

    @Override
    public boolean auth() {

        // Get Token
        // Adding Header Basic Auth Key

        if (true) {
            log("TOKEN : ASDF");
            return true;
        } else {
            return false;
        }
    }

    public String collect() {
        String data = "{\"data\":\"oauth_data_body\"}";
        log("collect : " + data);
        String formattedData = formatter.formattingData(data);
        log("formatted : " + data);
        log(formattedData);
        return formattedData;
    }

}
