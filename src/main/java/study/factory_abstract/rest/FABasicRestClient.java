package study.factory_abstract.rest;

import study.factory_abstract.sdc.FASDCFormatterFactory;

public class FABasicRestClient extends FARestClient {

    public FABasicRestClient(String address, int port, String id, String password) {
        this.address = address;
        this.port = port;
        this.id = id;
        this.password = password;
    }

    @Override
    public boolean auth() {

        // Generate Basic Auth Key
        // Adding Header Basic Auth Key

        if (true) {
            log("Basic AUTH : ALSKDJLSKADJ");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String collect() {

        String data = "{\"data\":\"basic_data_body\"}";
        log("collect : " + data);
        String formattedData = formatter.formattingData(data);
        log("formatted : " + data);
        log(formattedData);
        return formattedData;
    }

}
