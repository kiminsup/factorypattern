package study.factory_abstract.rest;

import study.enums.SDCType;
import study.factory_abstract.sdc.FASDCFormatterFactory;

public class FARestClientFactory {
    public static FARestClient getRestClient(String address, int port, String id, String password, SDCType sdcType) {
        FARestClient client = null;

        FASDCFormatterFactory formatterFactory = new FASDCFormatterFactory();

        switch (sdcType) {
            case VCENTER -> {
                client = new FAOAuth2RestClient(address, port, id, password);
                client.setName("VCENTER");
            }
            case NUTANIX -> {
                client = new FABasicRestClient(address, port, id, password);
                client.setName("NUTANIX");
            }
        }

        client.setFormatter(formatterFactory.getFormatter(sdcType));

        boolean auth = client.auth();
        if (auth) {
            client.log("AUTH SUCCESS");
            return client;
        } else
            return null;
    }
}
