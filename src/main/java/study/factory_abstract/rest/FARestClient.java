package study.factory_abstract.rest;

import study.factory_abstract.sdc.FASDCFormatter;

import java.util.Date;

public abstract class FARestClient {
    String name;
    String address;
    int port;
    String id;
    String password;
    FASDCFormatter formatter;

    public abstract boolean auth();

    public abstract String collect();

    public void log(String logString) {
        System.out.println(name + " : " + new Date().getTime() + " : " + logString);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFormatter(FASDCFormatter formatter) {
        this.formatter = formatter;
    }
}
