package study.factory_abstract.sdc;

import study.enums.SDCType;

public class FASDCFormatterFactory {
    public FASDCFormatter getFormatter(SDCType type) {
        switch (type) {
            case VCENTER -> {
                return new FAVCenterFormatter();
            }
            case NUTANIX -> {
                return new FANutanixFormatter();
            }
        }
        return null;
    }
}
