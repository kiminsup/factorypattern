package study.factory_abstract.sdc;

public class FANutanixFormatter implements FASDCFormatter {
    @Override
    public String formattingData(String collectData) {
        return "NUTANIX : " + collectData;
    }
}
