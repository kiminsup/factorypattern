package study.factory_abstract.sdc;

public class FAVCenterFormatter implements FASDCFormatter {
    @Override
    public String formattingData(String collectData) {
        return "VCENTER : " + collectData;
    }
}
