package study.factory_abstract.sdc;

public interface FASDCFormatter {
    String formattingData(String collectData);
}
